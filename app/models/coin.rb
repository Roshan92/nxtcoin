class Coin < ApplicationRecord

  def self.current_usd name
    coin_type = find_type(name)

    if coin_type == "Bitcoin"
      coin_rate = CoinRate.first
      if coin_rate.updated_at < 24.hours.ago
        usd = JSON.parse(RestClient.get("https://api.coindesk.com/v1/bpi/currentprice.json"))["bpi"]["USD"]["rate_float"] rescue 3856.0588
        coin_rate.update(usd: usd)
      else
        usd = coin_rate.usd.to_f
      end
    else
      usd = 1
    end
  end

  def self.coin_type name
    coin_type = find_type(name)

    if coin_type == "Bitcoin"
      return ["₿", "$", "USD"]
    elsif coin_type == "Tether"
      return ["$", "$", "USD"]
    else
      return ["","",""]
    end
  end

  def self.find_type name
    Coin.where(full_name: name).first.coin_type
  end

  def self.find_symbol name
    Coin.where(full_name: name).first.name
  end
end
