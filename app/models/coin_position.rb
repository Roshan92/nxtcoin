class CoinPosition < ApplicationRecord
  belongs_to :user

  def self.save_coin user_id, coin_no, coin_name
    coin_user = CoinPosition.find_by(user_id: user_id)

    if coin_user.blank?
      CoinPosition.create(user_id: user_id, "coin_#{coin_no}": coin_name)
    else
      CoinPosition.find_by(user_id: user_id).update("coin_#{coin_no}": coin_name)
    end
  end

end
