class Coins10Controller < ApplicationController

  def coin
    @name10 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 10, @name10)
    end

    @graph_data10 = history(@name10)
    @sidebar_data10 = summary(@name10)
    @coin_type10 = Coin.coin_type(@name10)
    @usd10 = Coin.current_usd(@name10)
  end

end
