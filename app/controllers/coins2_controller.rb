class Coins2Controller < ApplicationController

  def coin
    @name2 = params[:name]
    
    if logged_in?
      CoinPosition.save_coin(@current_user.id, 2, @name2)
    end

    @graph_data2 = history(@name2)
    @sidebar_data2 = summary(@name2)
    @coin_type2 = Coin.coin_type(@name2)
    @usd2 = Coin.current_usd(@name2)
  end

end
