class Coins4Controller < ApplicationController

  def coin
    @name4 = params[:name]
    
    if logged_in?
      CoinPosition.save_coin(@current_user.id, 4, @name4)
    end

    @graph_data4 = history(@name4)
    @sidebar_data4 = summary(@name4)
    @coin_type4 = Coin.coin_type(@name4)
    @usd4 = Coin.current_usd(@name4)
  end

end
