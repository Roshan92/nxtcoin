class Coins8Controller < ApplicationController

  def coin
    @name8 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 8, @name8)
    end

    @graph_data8 = history(@name8)
    @sidebar_data8 = summary(@name8)
    @coin_type8 = Coin.coin_type(@name8)
    @usd8 = Coin.current_usd(@name8)
  end

end
