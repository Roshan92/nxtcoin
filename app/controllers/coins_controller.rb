class CoinsController < ApplicationController
  # GET /coins
  # GET /coins.json
  # https://bittrex.com/api/v1.1/public/getmarkets
  # coins.each do |c|
  #   Coin.create(name: c.name, full_name: c.currency_name, coin_type: c.base_name, is_active: c.active)
  # end

  def coin
    @name = params[:name]
    if logged_in?
      CoinPosition.save_coin(@current_user.id, 1, @name)
    end

    @graph_data = history(@name)
    @sidebar_data = summary(@name)
    @coin_type = Coin.coin_type(@name)
    @usd = Coin.current_usd(@name)
  end

end
