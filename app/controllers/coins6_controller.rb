class Coins6Controller < ApplicationController

  def coin
    @name6 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 6, @name6)
    end

    @graph_data6 = history(@name6)
    @sidebar_data6 = summary(@name6)
    @coin_type6 = Coin.coin_type(@name6)
    @usd6 = Coin.current_usd(@name6)
  end

end
