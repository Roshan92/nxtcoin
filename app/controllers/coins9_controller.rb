class Coins9Controller < ApplicationController

  def coin
    @name9 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 9, @name9)
    end

    @graph_data9 = history(@name9)
    @sidebar_data9 = summary(@name9)
    @coin_type9 = Coin.coin_type(@name9)
    @usd9 = Coin.current_usd(@name9)
  end

end
