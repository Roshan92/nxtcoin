class Coins3Controller < ApplicationController

  def coin
    @name3 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 3, @name3)
    end

    @graph_data3 = history(@name3)
    @sidebar_data3 = summary(@name3)
    @coin_type3 = Coin.coin_type(@name3)
    @usd3 = Coin.current_usd(@name3)
  end

end
