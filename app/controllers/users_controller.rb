class UsersController < ApplicationController

  def create
    @user = User.new
    user = User.find_by(email: params[:email])

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      user.update(timezone: Time.now.to_s.split.last.insert(3,':'))
      redirect_to root_url
    elsif user.present?
      flash[:alert] = "Email exist, wrong password!"
      redirect_to root_url
    else
      @user = User.create(email: params[:email], password: params[:password])
      if @user.save
        session[:user_id] = @user.id
        @user.update(timezone: Time.now.to_s.split.last.insert(3,':'))
        CoinPosition.create(user_id: @user.id)
        redirect_to root_url
      else
        redirect_to root_url
      end
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  private

  def allowed_params
    params.require(:user).permit(:email, :password, :timezone)
  end
end
