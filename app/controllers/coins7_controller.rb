class Coins7Controller < ApplicationController

  def coin
    @name7 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 7, @name7)
    end

    @graph_data7 = history(@name7)
    @sidebar_data7 = summary(@name7)
    @coin_type7 = Coin.coin_type(@name7)
    @usd7 = Coin.current_usd(@name7)
  end

end
