class Coins5Controller < ApplicationController

  def coin
    @name5 = params[:name]

    if logged_in?
      CoinPosition.save_coin(@current_user.id, 5, @name5)
    end

    @graph_data5 = history(@name5)
    @sidebar_data5 = summary(@name5)
    @coin_type5 = Coin.coin_type(@name5)
    @usd5 = Coin.current_usd(@name5)
  end

end
