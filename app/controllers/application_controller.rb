class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  respond_to :js

  helper_method :logged_in?, :user_coin

  def coins
    if logged_in? && user_coin.coin_1.present?
      coin = user_coin.coin_1
      @name = coin
      @graph_data = history(@name)
      @sidebar_data = summary(@name)
      @coin_type = Coin.coin_type(@name)
      @usd = Coin.current_usd(@name)
    else
      coin = "Bitswift"
      @name = coin
      @graph_data = history(coin)
      @sidebar_data = summary(coin)
      @coin_type = ["₿", "$", "USD"]
      @usd = 3794.88
    end
  end

  def coins2
    if logged_in? && user_coin.coin_2.present?
      coin = user_coin.coin_2
      @name2 = coin
      @graph_data2 = history(@name2)
      @sidebar_data2 = summary(@name2)
      @coin_type2 = Coin.coin_type(@name2)
      @usd2 = Coin.current_usd(@name2)
    else
      coin = "GoldCoin"
      @name2 = coin
      @graph_data2 = history(coin)
      @sidebar_data2 = summary(coin)
      @coin_type2 = ["₿", "$", "USD"]
      @usd2 = 3794.88
    end
  end

  def coins3
    if logged_in? && user_coin.coin_3.present?
      coin = user_coin.coin_3
      @name3 = coin
      @graph_data3 = history(@name3)
      @sidebar_data3 = summary(@name3)
      @coin_type3 = Coin.coin_type(@name3)
      @usd3 = Coin.current_usd(@name3)
    else
      coin = "PotCoin"
      @name3 = coin
      @graph_data3 = history(coin)
      @sidebar_data3 = summary(coin)
      @coin_type3 = ["₿", "$", "USD"]
      @usd3 = 3794.88
    end
  end

  def coins4
    if logged_in? && user_coin.coin_4.present?
      coin = user_coin.coin_4
      @name4 = coin
      @graph_data4 = history(@name4)
      @sidebar_data4 = summary(@name4)
      @coin_type4 = Coin.coin_type(@name4)
      @usd4 = Coin.current_usd(@name4)
    else
      coin = "PotCoin"
      @name4 = coin
      @graph_data4 = @graph_data3
      @sidebar_data4 = @sidebar_data3
      @coin_type4 = ["₿", "$", "USD"]
      @usd4 = 3794.88
    end
  end

  def coins5
    if logged_in? && user_coin.coin_5.present?
      coin = user_coin.coin_5
      @name5 = coin
      @graph_data5 = history(@name5)
      @sidebar_data5 = summary(@name5)
      @coin_type5 = Coin.coin_type(@name5)
      @usd5 = Coin.current_usd(@name5)
    else
      coin = "PotCoin"
      @name5 = coin
      @graph_data5 = @graph_data3
      @sidebar_data5 = @sidebar_data3
      @coin_type5 = ["₿", "$", "USD"]
      @usd5 = 3794.88
    end
  end

  def coins6
    if logged_in? && user_coin.coin_6.present?
      coin = user_coin.coin_6
      @name6 = coin
      @graph_data6 = history(@name6)
      @sidebar_data6 = summary(@name6)
      @coin_type6 = Coin.coin_type(@name6)
      @usd6 = Coin.current_usd(@name6)
    else
      coin = "PotCoin"
      @name6 = coin
      @graph_data6 = @graph_data3
      @sidebar_data6 = @sidebar_data3
      @coin_type6 = ["₿", "$", "USD"]
      @usd6 = 3794.88
    end
  end

  def coins7
    if logged_in? && user_coin.coin_7.present?
      coin = user_coin.coin_7
      @name7 = coin
      @graph_data7 = history(@name7)
      @sidebar_data7 = summary(@name7)
      @coin_type7 = Coin.coin_type(@name7)
      @usd7 = Coin.current_usd(@name7)
    else
      coin = "PotCoin"
      @name7 = coin
      @graph_data7 = @graph_data3
      @sidebar_data7 = @sidebar_data3
      @coin_type7 = ["₿", "$", "USD"]
      @usd7 = 3794.88
    end
  end

  def coins8
    if logged_in? && user_coin.coin_8.present?
      coin = user_coin.coin_8
      @name8 = coin
      @graph_data8 = history(@name8)
      @sidebar_data8 = summary(@name8)
      @coin_type8 = Coin.coin_type(@name8)
      @usd8 = Coin.current_usd(@name8)
    else
      coin = "PotCoin"
      @name8 = coin
      @graph_data8 = @graph_data3
      @sidebar_data8 = @sidebar_data3
      @coin_type8 = ["₿", "$", "USD"]
      @usd8 = 3794.88
    end
  end

  def coins9
    if logged_in? && user_coin.coin_9.present?
      coin = user_coin.coin_9
      @name9 = coin
      @graph_data9 = history(@name9)
      @sidebar_data9 = summary(@name9)
      @coin_type9 = Coin.coin_type(@name9)
      @usd9 = Coin.current_usd(@name9)
    else
      coin = "PotCoin"
      @name9 = coin
      @graph_data9 = @graph_data3
      @sidebar_data9 = @sidebar_data3
      @coin_type9 = ["₿", "$", "USD"]
      @usd9 = 3794.88
    end
  end

  def coins10
    if logged_in? && user_coin.coin_10.present?
      coin = user_coin.coin_10
      @name10 = coin
      @graph_data10 = history(@name10)
      @sidebar_data10 = summary(@name10)
      @coin_type10 = Coin.coin_type(@name10)
      @usd10 = Coin.current_usd(@name10)
    else
      coin = "PotCoin"
      @name10 = coin
      @graph_data10 = @graph_data3
      @sidebar_data10 = @sidebar_data3
      @coin_type10 = ["₿", "$", "USD"]
      @usd10 = 3794.88
    end
  end

  def history name
    name = Coin.where(full_name: name).first.name rescue "BTC-FTC"
    Bittrex::History.get(name).select{|c| c.created_at > Date.today}.map{ |m| [(m.raw["TimeStamp"] + "+0000").to_time.localtime("#{logged_in? && @current_user.timezone.present? ? @current_user.timezone : '+00:00'}").strftime('%H:%M %p').to_s, m.price]}.uniq{|d| d.first}.take(10).reverse
  end

  def summary name
    name = Coin.where(full_name: name).first.name rescue "BTC-FTC"
    Bittrex::Summary.get(name)
  end

  private

  def logged_in?
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def user_coin
    CoinPosition.find_by(user_id: session[:user_id])
  rescue
    nil
  end

end
