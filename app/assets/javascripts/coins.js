var ready = function(){

// setInterval(refreshSensorReadings, 4000);

  var coins = ["Litecoin", "Dogecoin", "Vertcoin", "Peercoin", "Feathercoin", "ReddCoin", "NXT", "Dash", "PotCoin", "BlackCoin", "Einsteinium", "AuroraCoin", "ElectronicGulden", "GoldCoin", "SolarCoin", "PesetaCoin", "Groestlcoin", "Gulden", "RubyCoin", "WhiteCoin", "MonaCoin", "HempCoin", "EnergyCoin", "EuropeCoin", "NautilusCoin", "VeriCoin", "CureCoin", "Boolberry", "Monero", "CloakCoin", "StartCoin", "KoreCoin", "DigitalNote", "TrustPlus", "NAVCoin", "StealthCoin", "BitcoinDark", "ViaCoin", "Unobtanium", "PinkCoin", "I/OCoin", "CannabisCoin", "SysCoin", "NeosCoin", "Digibyte", "BURSTCoin", "ExclusiveCoin", "Bitswift", "DopeCoin", "BlockNet", "ArtByte", "Bytecent", "Magi", "Blitzcash", "BitBay", "BitShares", "FairCoin", "SpreadCoin", "vTorrent", "Ripple", "GameCredits", "Circuits of Value", "Nexus", "Counterparty", "BitBean", "GeoCoin", "FoldingCoin", "GridCoin", "Florin", "Nubits", "MonetaryUnit", "NewEconomyMovement", "CLAMs", "Diamond", "Gambit", "Sphere", "OkCash", "Synergy", "ParkByte", "CapriCoin", "Aeon", "Ethereum", "GlobalCurrencyReserve", "TransferCoin", "BitCrystals", "Expanse", "InfluxCoin", "OMNI", "AMP", "IDNI Agoras", "Lumen", "Bata", "Bitcoin", "ClubCoin", "Voxels", "EmerCoin", "Factom", "MaidSafe", "EverGreenCoin", "SaluS", "Radium", "Decred", "SafeExchangeCoin", "BitSend", "Verge", "Pivx", "Vcash", "Memetic", "STEEM", "2GIVE", "Lisk", "Project Decorum", "Breakout", "DigixDAO", "Digix DAO", "Waves", "Rise", "LBRY Credits", "SteemDollars", "Breakout Stake", "DT Token", "Ethereum Classic", "Stratis", "UnbreakableCoin", "Syndicate", "TRIG Token", "eBoost", "Verium", "Sequence", "Xaurum", "SingularDTV", "Augur", "Shift", "Ardor", "ZCoin", "Antshares", "Zcash", "Zclassic", "Internet Of People", "Darcrus", "Golos", "HackerGold", "Ubiq", "Komodo", "GBG", "Siberian Chervonets", "Ion", "Lomocoin", "Qwark", "Crown", "Swarm City Token", "Chronobank Time", "Melon", "Ark", "Dynamic", "Tokes", "Musicoin", "Databits", "Incent", "Byteball", "Golem", "Nexium", "Edgeless", "Legends", "Trustcoin", "Wings DAO", "iEx.ec", "Gnosis", "Guppy", "Lunyr", "Apx", "TokenCard", "Humaniq", "Aragon", "Siacoin", "Basic Attention Token", "Zencash", "FirstBlood", "Quantum Resistant Ledger", "CreditBit", "Patientory", "Mysterium", "Cofound.it", "Bancor", "Numeraire", "Time", "Status Network Token", "DECENT", "Elastic", "Monaco", "adToken", "FunFair", "TenX Pay Token", "Metal", "Storj", "AdEx", "OmiseGO", "Civic", "Particl", "Qtum"];

  $(function() {
    $(".search-field").autocomplete({
      source: coins,
      minLength: 2,
      delay: 200,
      autoFocus: true
    });
  });


  $('#name').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage').show();
    $('#line_chart').hide();

    $.rails.ajax({
      url: "/coin/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        $('#line_chart').show();
        $('#loadingmessage').hide();
      }
    });
  }));

  $('#name2').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage2').show();
    $('#line_chart2').hide();

    $.rails.ajax({
      url: "/coin2/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        $('#line_chart2').show();
        $('#loadingmessage2').hide();
      }
    });
  }));

  $('#name3').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage3').show();
    $('#line_chart3').hide();

    $.rails.ajax({
      url: "/coin3/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name3 :selected').val());
        $('#line_chart3').show();
        $('#loadingmessage3').hide();
      }
    });
  }));

  $('#name4').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage4').show();
    $('#line_chart4').hide();

    $.rails.ajax({
      url: "/coin4/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name4 :selected').val());
        $('#line_chart4').show();
        $('#loadingmessage4').hide();
      }
    });
  }));

  $('#name5').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage5').show();
    $('#line_chart5').hide();

    $.rails.ajax({
      url: "/coin5/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name5').val());
        $('#line_chart5').show();
        $('#loadingmessage5').hide();
      }
    });
  }));

  $('#name5a').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage5').show();
    $('#line_chart5').hide();

    $.rails.ajax({
      url: "/coin5a/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name5a :selected').val());
        $('#line_chart5').show();
        $('#loadingmessage5').hide();
      }
    });
  }));

  $('#name6').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage6').show();
    $('#line_chart6').hide();

    $.rails.ajax({
      url: "/coin6/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name6 :selected').val());
        $('#line_chart6').show();
        $('#loadingmessage6').hide();
      }
    });
  }));

  $('#name6a').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage6').show();
    $('#line_chart6').hide();

    $.rails.ajax({
      url: "/coin6a/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name6a :selected').val());
        $('#line_chart6').show();
        $('#loadingmessage6').hide();
      }
    });
  }));

  $('#name7').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage7').show();
    $('#line_chart7').hide();

    $.rails.ajax({
      url: "/coin7/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name7 :selected').val());
        $('#line_chart7').show();
        $('#loadingmessage7').hide();
      }
    });
  }));

  $('#name7a').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage7').show();
    $('#line_chart7').hide();

    $.rails.ajax({
      url: "/coin7a/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name7a :selected').val());
        $('#line_chart7').show();
        $('#loadingmessage7').hide();
      }
    });
  }));

  $('#name8').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage8').show();
    $('#line_chart8').hide();

    $.rails.ajax({
      url: "/coin8/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name8 :selected').val());
        $('#line_chart8').show();
        $('#loadingmessage8').hide();
      }
    });
  }));

  $('#name8a').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage8').show();
    $('#line_chart8').hide();

    $.rails.ajax({
      url: "/coin8a/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name8a').val());
        $('#line_chart8').show();
        $('#loadingmessage8').hide();
      }
    });
  }));

  $('#name9').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage9').show();
    $('#line_chart9').hide();

    $.rails.ajax({
      url: "/coin9/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name9 :selected').val());
        $('#line_chart9').show();
        $('#loadingmessage9').hide();
      }
    });
  }));

  $('#name9a').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage9').show();
    $('#line_chart9').hide();

    $.rails.ajax({
      url: "/coin9a/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name9a').val());
        $('#line_chart9').show();
        $('#loadingmessage9').hide();
      }
    });
  }));

  $('#name10').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage10').show();
    $('#line_chart10').hide();

    $.rails.ajax({
      url: "/coin10/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name10').val());
        $('#line_chart10').show();
        $('#loadingmessage10').hide();
      }
    });
  }));

  $('#name10a').on('autocompleteselect', (function(e,ui){
    $('#loadingmessage10').show();
    $('#line_chart10').hide();

    $.rails.ajax({
      url: "/coin10a/" + ui.item.value,
      type: "GET",
      dataType: 'script',
      success: function(result) {
        // console.log($('#name10a :selected').val());
        $('#line_chart10').show();
        $('#loadingmessage10').hide();
      }
    });
  }));

};


$(document).on('turbolinks:load', ready);
