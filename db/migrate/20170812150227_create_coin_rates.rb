class CreateCoinRates < ActiveRecord::Migration[5.1]
  def change
    create_table :coin_rates do |t|
      t.string :usd
      t.timestamps
    end
  end
end
