class CreateCoinPositions < ActiveRecord::Migration[5.1]
  def change
    create_table :coin_positions do |t|
      t.string :coin_1
      t.string :coin_2
      t.string :coin_3
      t.string :coin_4
      t.string :coin_5
      t.string :coin_6
      t.string :coin_7
      t.string :coin_8
      t.string :coin_9
      t.string :coin_10
      t.integer :user_id

      t.timestamps
    end
  end
end
