class CreateCoins < ActiveRecord::Migration[5.1]
  def change
    create_table :coins do |t|
      t.string :name
      t.string :full_name
      t.string :coin_type
      t.boolean :is_active, default: true
      t.timestamps
    end
  end
end
