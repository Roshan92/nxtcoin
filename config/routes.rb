Rails.application.routes.draw do
  root "home#index"

  resources :users
  get 'signup', to: 'users#new', as: 'signup'
  get 'logout', to: 'users#destroy', as: 'logout'

  get 'coin/:name' => 'coins#coin', as: "coin"
  get 'coin2/:name' => 'coins2#coin', as: "coin2"
  get 'coin3/:name' => 'coins3#coin', as: "coin3"
  get 'coin4/:name' => 'coins4#coin', as: "coin4"
  get 'coin5/:name' => 'coins5#coin', as: "coin5"
  get 'coin5a/:name' => 'coins5#coin'
  get 'coin6/:name' => 'coins6#coin', as: "coin6"
  get 'coin6a/:name' => 'coins6#coin'
  get 'coin7/:name' => 'coins7#coin', as: "coin7"
  get 'coin7a/:name' => 'coins7#coin'
  get 'coin8/:name' => 'coins8#coin', as: "coin8"
  get 'coin8a/:name' => 'coins8#coin'
  get 'coin9/:name' => 'coins9#coin', as: "coin9"
  get 'coin9a/:name' => 'coins9#coin'
  get 'coin10/:name' => 'coins10#coin', as: "coin10"
  get 'coin10a/:name' => 'coins10#coin'
end
